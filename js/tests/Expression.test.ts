import {Expression} from "../Expression";
import math = require("mathjs");
import {Unit} from "mathjs";

describe('', function () {
    let parameters: { description: string, input: string, expected: any }[] = [
        {
            description: "1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1 should equal 2",
            input: "1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1",
            expected: 28
        },
        {description: "2*2 should equal 4", input: "2*2", expected: 4},
        {description: "2/2 should equal 1", input: "2/2", expected: 1},
        {description: "2-2 should equal 0", input: "2-2", expected: 0},
        {description: "8+7*5*(6+8)*5 should equal 2458", input: "8+7*5*(6+8)*5", expected: 2458},
        {description: "(2+2) should equal 4", input: "(2+2)", expected: 4},
        {description: "(2+2) should equal 4", input: "(28* 72)", expected: 2016},
        {description: "(2+2)*(2+2) should equal 4", input: "(2+2)*(2+2)", expected: 16},
        {description: "(2+2)*(2+2) should equal 4", input: "(28*72)+(18* 12)+(48 *8)+(6* 72)", expected: 3048},
        {description: "((2+2)) should equal 4", input: "((2+2))", expected: 4},
        {description: "2+2-2 should equal 2", input: "2+2-2", expected: 2},
        {description: "2^2 should equal 4", input: "2^2", expected: 4},
        {description: "2-2^2 should equal -2", input: "2-2^2", expected: -2},
        {description: "e should equal e", input: "e", expected: Math.E},
        {description: "ln(e) should equal 1", input: "ln(e)", expected: 1},
        {description: "sin(pi/2) should equal 1", input: "sin(pi/2)", expected: 1},
        {description: "pi should equal pi", input: "pi", expected: Math.PI},
        {description: "cos(pi*2) should equal 1", input: "cos(pi*2)", expected: 1},
        {description: "tan(1) should equal 1", input: "tan(1)", expected: Math.tan(1)},
        {description: "45° should equal pi/4", input: "45°", expected: Math.PI / 4},
        {description: "log(5,10) should equal pi/4", input: "log(5,10)", expected: 1.4306765580733933},
        {description: "2² should equal 4", input: "2²", expected: 4},
        {description: "2³ should equal 8", input: "2³", expected: 8},
        {description: "sqrt(4) should equal 2", input: "sqrt(4)", expected: 2},
        {description: "root(3,8) should equal 2", input: "root(3,8)", expected: 2},
        {description: "fact(5) should equal 120", input: "fact(5)", expected: 120},
        {description: "5! should equal 120", input: "5!", expected: 120},
        {description: "0xF should equal 15", input: "0xF", expected: 15},
        {description: "0b10 should equal 2", input: "0b10", expected: 2},
        {description: "0b10*0xF should equal 30", input: "0b10*0xF", expected: 30},
        {description: "0xF / 0o12 should equal 1.5", input: "0xF/0o12", expected: 1.5},
        {description: "solve(4=x,x) should equal 4", input: "solve(4=x,x)", expected: 4},
        {description: "solve(4=x^2,x) should equal 4", input: "solve(4=x^2,x)", expected: [2, -2]},
        {description: "4+solve(4=x,x) should equal 8", input: "4+solve(4=x,x)", expected: 8},
        {
            description: "solveEquation(x^2+4-y,y) should equal 4+x^2",
            input: "solveEquation(x^2+4-y,y)",
            expected: "4+x^2"
        },
        {description: "2 should equal 2", input: "2", expected: 2},
        {description: "0 should equal 0", input: "0", expected: 0},
        {
            description: "solveEquation(x+y=1, 2*x=6, 4*z+y=6) should equal 4+x^2",
            input: "solveEquation(x+y=1, 2*x=6, 4*z+y=6)",
            expected: ['x=3', 'y=-2', 'z=2']
        },
        {
            description: "solveEquations(3*x=4x^2,x) should equal 4+x^2",
            input: "solveEquation(3*x=4x^2,x)",
            expected: [0, .75]
        },
        {description: "convertUnit(20,ft,m)", input: "convertUnit(20,ft,m)", expected: '6.096 m'},
        {
            description: "convertUnit(28,degC,degF)",
            input: "convertUnit(28,degC,degF)",
            expected: '82.39999999999992 degF'
        },
        {description: "elontime", input: "elontime", expected: 3},
        {description: "solve(x^2=5,x)", input: "solve(x^2=5,x)", expected: [2.23606797749979, -2.23606797749979]},
        {description: "2*(-10)", input: "2*(-10)", expected: -20},
        {description: "tuple(2,2)*2 to be [4,4]", input: "tuple(2,2)*2", expected: [4, 4]},
        {description: "fact(2)*fact(3) to be 12", input: "fact(2)*fact(3)", expected: 12},
        {description: "x^2=4 equals [2,-2]", input: "x^2=4", expected: [2, -2]},
        {description: "(x^2=4) makes [2, -2]", input: "(x^2=4)", expected: [2, -2]},
        {description: "(xyda^2=4) makes NaN", input: "(xyda^2=4)", expected: [2, -2]},
        {description: "2=2 makes true", input: "2=2", expected: true},
        {description: "2=3 makes false", input: "2=3", expected: false},
        {description: "2=3 makes false", input: "2_m", expected: '2 m'},
        {description: "2_m+2_m", input: "2_m/2_s", expected: '1 m / s'},
        {description: "nunit(2,m/s)*2_m", input: "nunit(2,m/s)*2_m", expected: '4 (m m) / s'},
        {description: "105elontime", input: "105elontime", expected: 315},
        {description: "2_m", input: "2_m", expected: '2 m'},
    ]
    for (let {description, input, expected} of parameters) {
        it(input + " should be " + expected, () => {
            let evaluate = Expression.evaluate(input, 1);

            // console.log("ev", evaluate)
            expect(evaluate).toEqual(expected)
        })
    }
    // console.log(Expression.maxIterations)


    // console.log(Expression.functions)
});
describe('produces error', function () {
    let parameters: { description: string, input: string, expected: any }[] = [
        {description: "(x^2=) makes NaN", input: "(x^2=)", expected: "An equation with only one part doesn't work"},
        {description: "(x^2=4y) makes NaN", input: "(x^2=4y)", expected: "System does not have a distinct solution"},
    ]
    for (let {description, input, expected} of parameters) {
        it(description, () => {
            expect(() => Expression.evaluate(input)).toThrowError(expected)
        })
    }
    // console.log(Expression.maxIterations)


    // console.log(Expression.functions)
});


describe('produces no errors', () => {
    let parameters: { description: string, input: string }[] = [
        {description: "105e+9 * 9", input: "105e+9 * 9"},
    ]
    for (let {description, input} of parameters) {
        it(description, () => {
            expect(() => Expression.evaluate(input))
        })
    }
})