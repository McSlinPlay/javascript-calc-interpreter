// import {UnitConverter} from "./helpers/UnitConverter";
import {ExpressionPart} from "./interfaces/ExpressionPart";
import {NumberResult} from "./NumberResult";
import {Tuple} from "./Tuple";
import {StringResult} from "./StringResult";
import {BooleanResult} from "./BooleanResult";
import {UnsupportedError} from "./errors/UnsupportedError";
// import validate = WebAssembly.validate;
import {all, create} from "mathjs";

let math = create(all, {})
// console.log("adding units")
math.createUnit("ly", "9460730777119.56 km")
math.createUnit("pc", "30856775812800 km")
math.createUnit("fur", "220 yd")
math.createUnit("fir", "90 lb")
math.createUnit("ftn", "14 days")

let nerdamer = require('nerdamer/all.min');

export class Expression extends ExpressionPart {

    private string: string
    private operator: string
    private parts: string[]
    private readonly _iteration: number;
    private readonly _userid: string | number;
    public static maxIterations = process.env.MAX_ITERATIONS || 20

    public static evaluate = (string: string, userid?: string | number): ExpressionPart | ExpressionPart[] => {
        // return math.evaluate(string)
        return new Expression(string, userid, 0).getFinalValue();
    };

    public getFinalValue(): any {
        const value = this.getValue();
        // console.log("pre return", value)
        if (value instanceof ExpressionPart) {
            return value.getFinalValue()
        } else if (Array.isArray(value)) {
            return value.map(part => part.getFinalValue())
        }
        return value
    }

    private constructor(string: string, userid?: string | number, iteration: number = 0) {
        super();
        string = string.replace(" ", "")
        this._userid = userid;
        if (iteration >= Expression.maxIterations) {
            throw new Error("Could not calculate a result for your input")
        }
        this.string = string
        this._iteration = iteration;
    }

    public getValue(): ExpressionPart | ExpressionPart[] {
        let number = this.string.indexOf("=");
        if (this.string.startsWith("_")) {
            // console.log(this.string)
            let numberResult = new NumberResult(this.string);
            // console.log(numberResult)
            return numberResult
        } else if (number > 0 && !this.string.startsWith("(") && !this.string.endsWith(")") && this.string.indexOf("=", number) !== -1) {
            // console.log("only one =", this.string)
            // check if only one variable is present
            let strings = this.string.split("=");
            if (strings.length === 2) {
                // console.log(strings)
                if (strings[0] === "" || strings[1] === "")
                    throw new Error("An equation with only one part doesn't work")

                let split = this.string.split(/[\W\d]/);
                // console.log(split)
                split = split.filter((value, index, self) => {
                    if (value === "") return false
                    return self.indexOf(value) === index
                })
                if (split.length === 1) {
                    let varname = split[0]
                    this.string = "solve(" + this.string + "," + varname + ")"
                    // console.log(this.string)
                } else if (split.length === 0) {
                    // console.log("bool")
                    let value2 = new Expression(strings[0], this._userid, this._iteration + 1).getValue();
                    let value3 = new Expression(strings[1], this._userid, this._iteration + 1).getValue();
                    // console.log(value2, value3)
                    let value1 = JSON.stringify(value2) === JSON.stringify(value3);
                    return new BooleanResult(value1)
                } else {
                    this.string = "solveEquation(" + this.string + ")"
                }
            } else {
                throw new Error("An equation with only one part doesn't work")
            }
        }
        if (this.isSplit()) {
            // console.log("is split")
            // Replace other number systems
            if (this.string.charAt(0) === "0" && this.string.charAt(1).match(/[xdbo]/)) {
                this.convertToDecimal()
            }
            if (Object.keys(Expression.constants).includes(this.string)) {
                return Expression.getConstant(this.string)
            }
            // Apply short functions
            for (let afterNumberOperator of Expression.getAfterNumberFunctions()) {
                // console.log(afterNumberOperator)
                if (this.string.endsWith(afterNumberOperator.name)) {
                    return afterNumberOperator.function(this.string)
                }
            }
            // Last fallback parse as float
            try {
                // console.log("fallback parse: ", this.string)
                // console.log("result", numberResult)
                // let matchArray = this.string.match(/[a-zA-Z]+/)
                let splitParts = this.string.split(/([&=_?[a-zA-Z]+)/g)
                splitParts = splitParts.filter(value => value.trim().length !== 0)
                if (splitParts.length > 1) {
                    let string = splitParts.join("*");
                    // console.log(string)
                    return new Expression(string, this._userid).getValue()
                } else {
                    return new NumberResult(this.string)
                }
            } catch (e) {
                return new Expression(this.string, this._userid, this._iteration + 1)
            }
        } else {
            // TODO split

            this.split()
            // console.log("split", this.parts)

            if (this.operator === undefined) {
                return new Expression(this.string, this._userid, this._iteration + 1).getValue()
            } else if (Object.keys(this.splittingFunctions).includes(this.operator)) {
                // console.log(this.operator)
                // console.log(this.operatsatuor, this.parts)
                return this.splittingFunctions[this.operator](...this.parts.map(value => {
                    // console.log(evaluate1)
                    // console.log("splited", value)
                    return new Expression(value, this._userid, this._iteration).getValue()
                }))
            } else {
                let func = Expression.getFunction(this.operator);
                if (func !== undefined)
                    if (func.function.length === this.parts.length || func.takesArray || (func.selfProcessing && func.function.length === this.parts.length + 1)) {
                        let result
                        let argumentsArray: any[] = []
                        if (!func.selfProcessing) {
                            argumentsArray.push(...this.parts.map(value => new Expression(value, this._iteration + 1).getValue()))
                        } else {
                            argumentsArray.push(...this.parts)
                        }
                        argumentsArray.push(this._userid)
                        // console.log(argumentsArray)
                        result = func.function(...argumentsArray)
                        // console.log("result hi", result)
                        return result
                        // }
                    } else {
                        throw new Error("You did not provide enough arguments for the function " + func.name)
                    }
                else
                    throw new Error("Couldn't find function " + this.operator + " in iteration " + this._iteration + " for input " + this.string)
            }
        }
    }

    /*private returnArrayOrNumber(result: number | number[] | string | string[] | boolean) {
        let r = result
        // console.log(result)
        if (Array.isArray(result)) {
            if (result.length === 1) {
                r = result[0]
            } else if (result.length === 0) {
                throw new Error("There is no result for your input")
            }
        }
        if (typeof r === 'string') {
            // console.log(r)
            try {
                let evaluate = Expression.evaluate(r, this._userid);
                // console.log(evaluate)
                if (typeof evaluate === 'number')
                    if (!isNaN(<number>evaluate))
                        return evaluate
            } catch (e) {

            }
        }
        /!*if (this._iteration === 0) {
            return r.toString()
        }*!/
        return r
    }*/

    private static removeNonParenthesis(s: string) {
        let onlyParenthesis = ""
        for (let i = 0; i < s.length; i++) {
            let charAt = s.charAt(i);
            if (charAt === "(" || charAt === ")") {
                onlyParenthesis += charAt
            }
        }
        return onlyParenthesis;
    }

    private static removeParenthesis(s): string {
        let stack: {char: string, i: number}[] = [];

        let open = {
            '{': '}',
            '[': ']',
            '(': ')'
        };

        let closed = {
            '}': true,
            ']': true,
            ')': true
        }

        let pPairs: {i: number, j: number}[] = []

        for (let i = 0; i < s.length; i++) {

            let char = s[i];

            if (open[char]) {
                stack.push({
                    char, i
                });
            } else if (closed[char]) {
                let pop = stack.pop();
                if (pop === undefined) continue
                if (open[pop.char] !== char) throw new Error("Found not matching parentheses");
                else {
                    pPairs.push({
                        i: pop.i,
                        j: i
                    })
                }
            }
        }
        let length = pPairs
            .sort((a, b) => a.i - b.i)
            .filter(pPair => (s.length - pPair.i - 1) === pPair.j).length;
        s = s.substring(length, s.length - length)
        return s
    }

    private split() {
        let expressionString = this.string

        // console.log("normal splitting")
        let openParenthesis = 0
        let leadingParentheses = 0
        let lastParenthesis = -1
        let encounteredClosing = false
        expressionString = Expression.removeParenthesis(expressionString)
        for (let i = 0; i < this.string.length; i++) {
            let char = this.string[i];
            if (char === '(') {
                openParenthesis++
            } else if (char === ')') {
                openParenthesis--
            } else if (openParenthesis === 0) {
                for (let splitter of this.splitters) {
                    if (splitter.splitter === char) {
                        splitter.firstIndex = i
                    }
                }
            }
        }
        this.string = expressionString
        let splitIndex: number = -1
        for (let splitter of this.splitters) {
            if (splitter.firstIndex !== -1) {
                this.operator = splitter.splitter
                splitIndex = splitter.firstIndex
                break
            }
        }
        if (splitIndex > -1) {
            let s = expressionString.substring(0, splitIndex);
            let s1 = expressionString.substring(splitIndex + 1, expressionString.length);
            this.parts = [s, s1]
            // console.log(this.parts)
            return;
        }
        for (let func of Expression.functions) {
            if (func.afterNumber === undefined || !func.afterNumber) {
                if (expressionString.startsWith(func.name + "(") && expressionString.endsWith(")")) {
                    // complex operator (with parenthesis) needs different splitting
                    let indexOf = expressionString.indexOf("(")
                    expressionString = expressionString.substring(indexOf + 1, expressionString.length - 1)
                    let openParenthesis = 0
                    let lastComma = 0
                    let parts = []
                    for (let i = 0; i < expressionString.length; i++) {
                        let s2 = expressionString.charAt(i);
                        if (s2 === "[") openParenthesis++
                        else if (s2 === "]") openParenthesis--
                        else if (s2 === "," && openParenthesis === 0) {
                            let s3 = expressionString.substring(lastComma, i);
                            lastComma = i + 1
                            parts.push(s3)
                        }
                    }
                    if (lastComma !== 0) {
                        let s4 = expressionString.substring(lastComma, expressionString.length);
                        parts.push(s4)
                    }
                    if (parts.length === 0) {
                        parts.push(expressionString)
                    }
                    this.parts = parts
                    this.operator = func.name
                    this.string = expressionString
                    return;
                }
            }
        }
        this.parts = [expressionString]
        return;
    }

    private convertToDecimal() {
        let radix = 10
        if (this.string.charAt(1) === "x") {
            // Hexadecimal
            radix = 16
        } else if (this.string.charAt(1) === "d") {
            // Decimal
            radix = 10
        } else if (this.string.charAt(1) === "b") {
            // Binary
            radix = 2
        } else if (this.string.charAt(1) === "o") {
            // Octal
            radix = 8
        } else {

        }
        let endCutOff = 0
        for (let afterNumberOperator of Expression.getAfterNumberFunctions()) {
            if (this.string.endsWith(afterNumberOperator.name)) {
                endCutOff = afterNumberOperator.name.length + 1
                break
            }
        }
        let firstPart = this.string.substring(2, this.string.length - endCutOff)
        let lastPart = this.string.substring(this.string.length - endCutOff)
        // console.log(firstPart, lastPart)
        this.string = parseInt(firstPart, radix) + lastPart + "";

    }

    private isSplit(): boolean {
        for (let func of Expression.functions) {
            let key = func.name
            if (key !== "" && this.string.includes(key + "(")) {
                return false
            }
        }
        for (let splitter of this.splitters) {
            if (this.string.includes(splitter.splitter)) {
                return false
            }
        }
        return true
    }

    private splitters: { splitter: string, firstIndex: number }[] = [
        {
            splitter: "-",
            firstIndex: -1
        },
        {
            splitter: "+",
            firstIndex: -1
        },
        {
            splitter: "/",
            firstIndex: -1
        },
        {
            splitter: "*",
            firstIndex: -1
        },
        {
            splitter: "^",
            firstIndex: -1
        }
    ]

    private splittingFunctions = {
        "+": (a: ExpressionPart, b: ExpressionPart): ExpressionPart => {
            // console.log("a",a)
            // console.log("b",b)
            let expressionPart = a.add(b);
            // console.log(expressionPart)
            return expressionPart
        },
        "-": (a: ExpressionPart, b: ExpressionPart): ExpressionPart => {
            if (a === undefined || a === null || (a instanceof NumberResult && isNaN((<NumberResult>a).getValue())) && !Array.isArray(a)) a = new NumberResult(0)
            return a.subtract(b)
        },
        "*": (a: ExpressionPart, b: ExpressionPart): ExpressionPart => {
            // console.log((<NumberResult>a).unit)
            // console.log((<NumberResult>b).unit)
            let expressionPart = a.times(b);
            // console.log((<NumberResult>expressionPart).unit)
            return expressionPart
        },
        "/": (a: ExpressionPart, b: ExpressionPart): ExpressionPart => {
            return a.divideBy(b)
        },
        "^": (a: ExpressionPart, b: ExpressionPart): ExpressionPart => {
            return a.power(b)
        }
    }

    public static getAfterNumberFunctions(): func[] {
        return Expression.functions.filter(value => {
            return value.afterNumber !== undefined && value.afterNumber === true;
        });
    }

    private static fact = (op) => {
        // Lanczos Approximation of the Gamma Function
        // As described in Numerical Recipes in C (2nd ed. Cambridge University Press, 1992)
        const z = op + 1;
        const p = [1.000000000190015, 76.18009172947146, -86.50532032941677, 24.01409824083091, -1.231739572450155, 1.208650973866179E-3, -5.395239384953E-6];

        const d1 = Math.sqrt(2 * Math.PI) / z;
        let d2 = p[0];

        for (let i = 1; i <= 6; ++i)
            d2 += p[i] / (z + i);

        let d3 = Math.pow((z + 5.5), (z + 0.5));
        let d4 = Math.exp(-(z + 5.5));

        let d = d1 * d2 * d3 * d4;

        return Math.round((d + Number.EPSILON) * 100) / 100;
    }

    private static userVariables = {}

    private static functions: func[] = [
        {
            name: "ln",
            description: [{
                command: "ln(n)",
                description: "Calculates the natural logarithm of ***n***"
            }],
            selfProcessing: false,
            function: (a): NumberResult => {
                return new NumberResult(Math.log(a))
            }
        },
        {
            name: "sin",
            description: [{
                command: "sin(x)",
                description: "Calculates the sinus of ***x***. ***x*** is in radians"
            }],
            selfProcessing: false,
            function: (a): NumberResult => {
                return new NumberResult(Math.sin(a))
            }
        },
        {
            name: "cos",
            description: [{
                command: "cos(x)",
                description: "Calculates the cosine of ***x***. ***x*** is in radians"
            }],
            selfProcessing: false,
            function: (a): NumberResult => {
                return new NumberResult(Math.cos(a))
            }
        },
        {
            name: "tan",
            description: [{
                command: "tan(x)",
                description: "Calculates the tangent of ***x***. ***x*** is in radians"
            }],
            selfProcessing: false,
            function: (a): NumberResult => {
                return new NumberResult(Math.tan(a))
            }
        },
        {
            name: "cot",
            description: [{
                command: "cot(x)",
                description: "Calculates the cotangent of ***x***. ***x*** is in radians"
            }],
            selfProcessing: false,
            function: (a): NumberResult => {
                return new NumberResult(1 / Math.tan(a))
            }
        },
        {
            name: "log",
            description: [{
                command: "log(base, x)",
                description: "Calculates the logarithm of ***n*** to the base"
            }],
            selfProcessing: false,
            function: (base, exp): NumberResult => {
                return new NumberResult(Math.log(exp) / Math.log(base))
            }
        },
        {
            name: "root",
            description: [{
                command: "root(n, x)",
                description: "Calculates the *n*th root of ***x***"
            }],
            selfProcessing: false,
            function: (a, b) => {
                return new NumberResult(b ** (1 / a))
            }
        },
        {
            name: "sqrt",
            description: [{
                command: "sqrt(x)",
                description: "Calculates the square root of ***x***"
            }],
            selfProcessing: false,
            function: (a) => {
                return new NumberResult(Math.sqrt(a))
            }
        },
        {
            name: "fact",
            description: [{
                command: "fact(x)",
                description: "Calculates the factorial of ***x***. Equal to x!"
            }],
            selfProcessing: false,
            function: (op) => {
                if (op instanceof NumberResult) {
                    // console.log(op)
                    let value = Expression.fact(op.getValue());
                    // console.log("fact result", value)
                    return new NumberResult(value)
                } else {
                    throw new UnsupportedError()
                }
            }
        },
        {
            name: "solve",
            description: [
                {
                    command: "solve(equation, var)",
                    description: "Solve an ***equation*** for a given variable ***var***"
                }
            ],
            selfProcessing: true,
            function: (a, b, user) => {
                // console.error("solve", a, b)
                let toString = nerdamer.solve(a, b).toString();
                toString = toString.substring(1, toString.length - 1).split(",")
                toString = toString.map(value => new Expression(value, user).getValue())
                // console.log("solved", toString)
                // console.log("solve", toString)
                let tuple = new Tuple(...toString);
                // console.log(tuple)
                return tuple
            }
        },
        {
            name: "solveEquation",
            description: [
                {
                    command: "solveEquation(equation, var)",
                    description: "Also solves an ***equation*** for a given variable ***var***. But in addition it can handle non numeric results."
                },
                {
                    command: "solveEquations([equation1, equation2, ...])",
                    description: "Solve a system of linear equations. Gives the values for the different variables.",
                }
            ],
            selfProcessing: true,
            takesArray: true,
            function: (...a: string[] | number[]): ExpressionPart[] | ExpressionPart => {
                a.splice(a.length - 1, 1)
                if (a.length === 2 && typeof a[1] === 'string' && a[1].match(/^[a-z]+$/)) {
                    // console.log("solvee", toString1)
                    return new Tuple(...nerdamer.solveEquations(...a).toString().split(",").map(value => {
                        // console.log("value", value)
                        try {
                            return new NumberResult(value);
                        } catch (e) {
                            if (typeof value === "string" && !value.match(/[*+-]/) && value.split("").filter(value1 => value1 === "/").length === 1) {
                                return new Expression(value).getValue()
                            }
                            return new StringResult(value)
                        }
                    }))
                } else {
                    let toString = nerdamer.solveEquations(a)
                    let array: ExpressionPart[] = []
                    for (let toStringElement of toString) {
                        array.push(new StringResult(toStringElement[0] + "=" + toStringElement[1]))
                    }
                    return array;
                }
            }
        },
        {
            name: "°",
            afterNumber: true,
            selfProcessing: false,
            function: (string) => {
                let s = string.substring(0, string.length - 1);
                let number = parseFloat(s);
                return new NumberResult(number * Math.PI / 180)
            }
        },
        {
            name: "²",
            afterNumber: true,
            selfProcessing: false,
            function: (string) => {
                let number = parseFloat(string);
                return new NumberResult(number ** 2)
            }
        },
        {
            name: "³",
            afterNumber: true,
            selfProcessing: false,
            function: (string) => {
                let number = parseFloat(string);
                return new NumberResult(number ** 3)
            }
        },
        {
            name: "!",
            afterNumber: true,
            selfProcessing: false,
            function: (string) => {
                let number = parseFloat(string)
                return new NumberResult(Expression.fact(number))
            }
        },
        {
            name: "convertUnit",
            selfProcessing: true,
            description: [{
                command: "convertUnit(value, oldUnit, newUnit)",
                description: "Converts the ***value*** from the ***oldUnit*** to the provided ***newUnit***. For a list of available units see https://gitlab.com/McSlinPlay/javascript-calc-interpreter/-/wikis/units"
            }],
            function: (string, oldUnit, newUnit, user) => {
                // console.log("Convert", string, oldUnit, newUnit, user)
                let evaluate = Expression.evaluate(string, user);
                oldUnit = oldUnit.trim()
                newUnit = newUnit.trim()
                try {
                    // console.log("evaluate", evaluate)
                    if (typeof evaluate === 'string') {
                        throw new Error("Please enter a valid value or expression that can be evaluated to a numeric value")
                    } else if (evaluate instanceof NumberResult) {
                        // console.log(evaluate, oldUnit, newUnit)
                        // let s1 = UnitConverter.convert(evaluate.getValue(), oldUnit, newUnit);
                        let s1 = math.unit(evaluate.getValue(), oldUnit).to(newUnit).toString();
                        // console.log("Converted", s1)
                        return new StringResult(s1)
                    } else if (typeof evaluate === "number") {
                        let s1 = math.unit(evaluate, oldUnit).to(newUnit).toString();
                        // console.log("Converted", s1)
                        return new StringResult(s1)
                    } else {
                        throw new Error("Please use a valid datatype")
                    }
                } catch (e) {
                    if (e.message.startsWith("Unsupported unit")) {
                        let number = e.message.indexOf(",");
                        let s = e.message.substring(17, number);
                        throw new Error("Unsupported unit " + s)
                    } else {
                        throw e
                    }
                }
            }
        },
        {
            name: "tuple",
            selfProcessing: false,
            takesArray: true,
            function: (...a: ExpressionPart[]): Tuple => {
                let pop = a.pop();
                // console.log("Splice",splice)
                // console.log("pop", a)
                // let expressions = a.map(value => new Expression(value));
                return new Tuple(...a)
            }
        },
        {
            name: "setvar",
            selfProcessing: true,
            needsUser: true,
            description: [{
                command: "setvar(varName, value)",
                description: "Saves a variable and its value for you. Only temporally so don't expect it to be there tomorrow."
            }],
            function: (name, value, user) => {
                let evaluate = Expression.evaluate(value, user);
                Expression.userVariables[user] = {}
                Expression.userVariables[user][name] = evaluate
                return new StringResult(name + " = " + evaluate)
            }
        },
        {
            name: "var",
            selfProcessing: true,
            needsUser: true,
            description: [{
                command: "var(varname)",
                description: "Gets the value for a previously saved variable"
            }],
            function: (varname, user) => {
                // console.log(Expression.userVariables)
                if (Expression.userVariables[user] === undefined || Object.keys(Expression.userVariables[user]).length === 0) {
                    throw new Error("You don't have any saved variables")
                }
                let userVariableElement = Expression.userVariables[user][varname];
                if (userVariableElement === undefined) {
                    throw new Error("You don't have a varibale called " + varname)
                }
                return new StringResult(userVariableElement)
            }
        },
        {
            name: "nunit",
            description: [{
                command: "nunit(number/value, unit)",
                description: "Creates a value with unit for further calculations"
            }],
            selfProcessing: true,
            needsUser: false,
            takesArray: false,
            afterNumber: false,
            function: (number, unit) => {
                // console.log("nunit", number, unit)
                let numberResult = new NumberResult(number, unit);
                // console.log(numberResult)
                return numberResult
            }
        }
    ]

    public static getHelp() {
        return this.functions
            .filter(value => value.description !== undefined)
            .map(value => value.description)
            // @ts-ignore
            .flat(1)
    }

    public static getConstants() {
        return this.constants
    }

    public static getConstant(name: string) {
        if (Object.keys(this.constants).includes(name)) {
            return new NumberResult(this.constants[name])
        }
    }

    private static constants = {
        "pi": Math.PI,
        "e": Math.E,
        "elontime": 3,
        "infinity": Infinity,
        "g": 9.80665,
        "G": 0.00000000006674
    }

    public static getFunction(name: string) {
        for (let fun of this.functions) {
            if (fun.name === name) {
                return fun
            }
        }
    }

    public static addFunction(func: func) {
        this.functions.push(func)
    }

    public static addConstant(name: string, value: number | number[] | string | string[]) {
        this.constants[name] = value
    }

    add(expressionPart: ExpressionPart): ExpressionPart {
        return undefined;
    }

    divideBy(expressionPart: ExpressionPart): ExpressionPart {
        return undefined;
    }

    subtract(expressionPart: ExpressionPart): ExpressionPart {
        return undefined;
    }

    times(expressionPart: ExpressionPart): ExpressionPart {
        return undefined;
    }

    power(expressionPart: ExpressionPart): ExpressionPart {
        return undefined;
    }

}

type func = {
    name: string
    description?: commandHelp[]
    needsUser?: boolean
    takesArray?: boolean
    afterNumber?: boolean
    selfProcessing: boolean
    function: (...a) => ExpressionPart | ExpressionPart[]
}

type commandHelp = {
    command: string,
    description: string
}

