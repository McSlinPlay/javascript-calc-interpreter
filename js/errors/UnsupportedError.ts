export class UnsupportedError extends Error {
    constructor() {
        super("This operation is not supported.");
    }
}