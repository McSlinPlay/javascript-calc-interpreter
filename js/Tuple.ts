import {ExpressionPart} from "./interfaces/ExpressionPart";
import {NumberResult} from "./NumberResult";
import {UnsupportedError} from "./errors/UnsupportedError";

export class Tuple extends ExpressionPart {

    private readonly value: ExpressionPart[]

    constructor(...value: ExpressionPart[]) {
        super();
        this.value = value
    }

    getValue() {
        if (this.value.length === 1) return this.value[0]
        return this.value.map(value => {
            if (value instanceof NumberResult)
                return value;
            else return value
        })
    }

    getFinalValue(): any {
        if (this.value.length === 1) {
            // console.log("only one")
            let finalValue = this.value[0].getFinalValue();
            // console.log(finalValue)
            return finalValue
        }
        // console.log("more than one")
        return this.value.map(value => value.getFinalValue())
    }

    private doNumberCalc(expressionPart: ExpressionPart, combiner: (a: ExpressionPart, b: ExpressionPart) => ExpressionPart): ExpressionPart[] {
        if (expressionPart instanceof NumberResult) {
            let expressionParts = this.value.filter(value => !(value instanceof NumberResult));
            if (expressionParts.length === 0) {
                return this.value.map(value => combiner(value, expressionPart));
            }
        }/* else if (expressionPart instanceof Tuple) {
            let value = (<Tuple>expressionPart).getValue();
            if (Array.isArray(value)) {
                value.filter(value1 => value1 instanceof NumberResult)
                let map = value.map(value1 => combiner(this.value, value1.getValue()))
                return new Tuple(...map)
            } else {
                return combiner(this.value, value.getValue())
            }
        }*/
        throw new UnsupportedError()
    }

    add(expressionPart: ExpressionPart): ExpressionPart {
        return new Tuple(...this.doNumberCalc(expressionPart, ((a, b) => a.add(b))))
    }

    divideBy(expressionPart: ExpressionPart): ExpressionPart {
        return new Tuple(...this.doNumberCalc(expressionPart, ((a, b) => a.divideBy(b))))
    }

    subtract(expressionPart: ExpressionPart): ExpressionPart {
        return new Tuple(...this.doNumberCalc(expressionPart, ((a, b) => a.subtract(b))))
    }

    times(expressionPart: ExpressionPart): ExpressionPart {
        return new Tuple(...this.doNumberCalc(expressionPart, ((a, b) => a.times(b))))
    }

    power(expressionPart: ExpressionPart): ExpressionPart {
        return new Tuple(...this.doNumberCalc(expressionPart, ((a, b) => a.power(b))))
    }

    toString() {
        return this.value.map(value => value.toString()).join(", ")
    }

}