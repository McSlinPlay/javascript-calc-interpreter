import {ExpressionPart} from "./interfaces/ExpressionPart";
import {UnsupportedError} from "./errors/UnsupportedError";

export class Raw extends ExpressionPart {

    private readonly value: string

    constructor(value) {
        super();
        this.value = value
    }

    getValue(): string {
        return this.value
    }

}