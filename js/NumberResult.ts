import {ExpressionPart} from "./interfaces/ExpressionPart";
import {Tuple} from "./Tuple";
import {UnsupportedError} from "./errors/UnsupportedError";
import * as math from "mathjs";
import {isNumeric, Unit} from "mathjs";

export class NumberResult extends ExpressionPart {
    private _unit: math.Unit = null;
    private _actualUnit: boolean = false;

    set unit(value: math.Unit) {
        this._unit = value;
        this._unit = value;
        this._actualUnit = true
    }

    get unit(): math.Unit {
        return this._unit;
    }

    add(expressionPart: ExpressionPart): ExpressionPart {
        if (this.sameUnitAs(expressionPart)) {
            return this.doNumberCalc(
                expressionPart,
                (a, b) => new NumberResult(a + b),
                (a, b) => a
            );
        }
    }

    private sameUnitAs(expressionPart: ExpressionPart) {
        if (this.unit === undefined && (<NumberResult>expressionPart).unit === undefined) return true
        if (this.unit.toString().length === 0 || ((<NumberResult>expressionPart).unit.toString().length === 0)) {
            return true;
        }
        return expressionPart instanceof NumberResult && expressionPart.unit !== undefined && expressionPart.unit.equals(this._unit) ||
            // @ts-ignore
            expressionPart instanceof Tuple && Array.isArray(expressionPart.getValue()) && expressionPart.getValue().filter(value => {
                if (value instanceof NumberResult && value.unit !== undefined) {
                    return value.unit.equals(this._unit)
                }
                return false
            }).length === 0
    }

    private doNumberCalc(expressionPart: ExpressionPart, combiner: (a: number, b: number) => NumberResult, unitCombiner: (a: math.Unit, b: math.Unit) => Unit) {
        // console.log("calcing")
        if (expressionPart instanceof NumberResult) {
            let result = combiner(this.value, expressionPart.getValue());
            if (expressionPart.unit !== undefined && this.unit !== undefined)
                result.unit = unitCombiner(this.unit, expressionPart.unit)
            return result
        } else if (expressionPart instanceof Tuple) {
            let value = (<Tuple>expressionPart).getValue();
            if (Array.isArray(value)) {
                value.filter(value1 => value1 instanceof NumberResult)
                let map = value.map(value1 => {
                    let part = combiner(this.value, value1.getValue());
                    if (!(value1 instanceof NumberResult) || value1.unit !== undefined && this.unit !== undefined)
                        part.unit = unitCombiner(this.unit, (<NumberResult>value1).unit)
                    return part;
                })
                return new Tuple(...map)
            } else {
                if (value instanceof NumberResult) {
                    let value2 = value.getValue();
                    let numberResult = combiner(this.value, value2);
                    if (value.unit !== undefined && this.unit !== undefined)
                        numberResult.unit = unitCombiner(this.unit, value.unit)
                    return numberResult
                }
            }
        }
        // console.log("value",expressionPart)
        throw new UnsupportedError()
    }

    divideBy(expressionPart: ExpressionPart): ExpressionPart {
        let numberCalc = this.doNumberCalc(expressionPart, (a, b) => new NumberResult(a / b), (a, b) => a.divide(b)/*math.evaluate(a.toString() + "/" + b.toString()).unit*/);
        return numberCalc;
    }

    subtract(expressionPart: ExpressionPart): ExpressionPart {
        if (this.sameUnitAs(expressionPart))
            return this.doNumberCalc(expressionPart, (a, b) => new NumberResult(a - b), (a, b) => a);
    }

    times(expressionPart: ExpressionPart): ExpressionPart {
        return this.doNumberCalc(expressionPart, (a, b) => new NumberResult(a * b), (a, b) => a.multiply(b));
    }

    private readonly value: number

    constructor(value: number | string, unit?: string) {
        super();
        // console.log("value", value)
        // console.log("unit", unit)
        if (typeof value === "string") {
            if (value.match(/^[\d.,]*$/)) {
                this.value = parseFloat(value)
            } else if (value.includes("_")) {
                // console.log("hello")
                let strings = value.split("_", 2);
                strings = strings.filter(value => value.trim().length !== 0)
                // console.log(strings)
                if (strings.length === 1) {
                    // console.log(strings[0])
                    if (isNumeric(strings[0])) {
                        // console.log('value')
                        this.value = parseFloat(strings[0])
                    } else {
                        // console.log('unit')
                        this.value = 1
                        this._unit = math.unit(strings[0])
                        this._actualUnit = true;
                        // console.log(this.toString())
                    }
                } else {
                    this.value = parseFloat(strings[0])
                    this._unit = math.unit(strings[1])
                    this._actualUnit = true;
                }
            } else
                throw new Error("Provided argument does not match expected argument format")
        } else
            this.value = value
        if (unit !== undefined) {
            this._unit = math.unit(unit)
            this._actualUnit = true;
        }
        if (this._unit == null) {
            this._unit = math.unit("")
        }
    }

    getValue(): number {
        return this.value
    }

    getFinalValue(): any {
        if (this._unit !== undefined && this._unit != null && this._unit.toString().length > 0) {
            // console.log(this._unit)
            // console.log("to string with unit")
            return this.toString()
        } else {
            return this.value
        }
    }

    power(expressionPart: ExpressionPart): ExpressionPart {
        return this.doNumberCalc(expressionPart, (a, b) => new NumberResult(a ** b), (a, b) => a.pow(b));
    }


    /*sqrt(expressionPart: ExpressionPart): ExpressionPart {
        return this.doNumberCalc(expressionPart, (a, b) => new NumberResult(b ** (1 / a)), (a, b) => b.);
    }*/

    toString() {
        if (this._actualUnit === true && this._unit.toString().length > 0) {
            // console.log("to string with unit")
            return String(this.value + " " + this._unit.toString())
        } else return String(this.value)
    }

}