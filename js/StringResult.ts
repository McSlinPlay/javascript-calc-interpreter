import {ExpressionPart} from "./interfaces/ExpressionPart";
import {UnsupportedError} from "./errors/UnsupportedError";

export class StringResult extends ExpressionPart {

    private readonly value: string

    constructor(value: string) {
        super();
        this.value = value
    }

    getValue(): string {
        return this.value
    }

}