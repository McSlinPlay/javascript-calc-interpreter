import {UnsupportedError} from "../errors/UnsupportedError";

export abstract class ExpressionPart {
    abstract getValue(): any

    add(expressionPart: ExpressionPart): ExpressionPart {
        throw new UnsupportedError()
    }

    subtract(expressionPart: ExpressionPart): ExpressionPart {
        throw new UnsupportedError()
    }

    times(expressionPart: ExpressionPart): ExpressionPart {
        throw new UnsupportedError()
    }

    divideBy(expressionPart: ExpressionPart): ExpressionPart {
        throw new UnsupportedError()
    }

    power(expressionPart: ExpressionPart): ExpressionPart {
        throw new UnsupportedError()
    }

    sqrt(expressionPart: ExpressionPart): ExpressionPart {
        throw new UnsupportedError()
    }

    getFinalValue() {
        return this.getValue()
    }

}