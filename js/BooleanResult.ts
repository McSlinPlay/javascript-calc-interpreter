import {ExpressionPart} from "./interfaces/ExpressionPart";
import {UnsupportedError} from "./errors/UnsupportedError";

export class BooleanResult extends ExpressionPart {

    private readonly value: boolean;

    constructor(value: boolean) {
        super();
        this.value = value;
    }

    getValue(): boolean {
        return this.value
    }

}