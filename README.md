# Javascript Calc Interpreter

![pipeline-badge](https://gitlab.com/McSlinPlay/javascript-calc-interpreter/badges/master/pipeline.svg)
![pipeline-badge](https://img.shields.io/npm/dw/javascript-calc-interpreter.svg)

Util to parse expressions in string form and return the evaluated result.
This util is case-insensitive.

## How to Install

`npm install javascript-calc-interpreter --save`

## Usage

````typescript
const {Expression} = require('javascript-calc-interpreter')
Expression.evaluate("2+2")
````

## Get Help

````typescript
import {Expression} from "./Expression";

Expression.getHelp()
````

This returns an array with multiple objects that all have a command and description field.

## Configuration

Setting the maximum amount of iteration before the parsing is cancelled

````typescript
import {Expression} from "./Expression"

Expression.maxIterations = 30
````

## Adding your own functions

You can add your own functions and constants.

Functions:

You can choose the amount of arguments the function can take. 
If you set `selfProcessing` to `false` the input will be run through the interpreter before it is passed to the function, if not you will get the raw input.

````typescript
import {Expression} from "./Expression";

Expression.addFunction({
    name: "name",
    selfProcessing: false,
    function: (a, b) => a + b
})
````
#### Additional configurations for your custom functions

Set `takesArray` to `true` if your function can take a variable amount of arguments

If you set `description` your function will be discoverable by the `getHelp()` method
It takes an array of objects that have the format
````json5
{
    command: "function(arguments)",
    description: "Describe what your function does"
}
````

If you want to add a constant you can do it like this

````typescript
import {Expression} from "./Expression";

Expression.addConstant("pi", Math.PI)
````